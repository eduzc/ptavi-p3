#!/usr/bin/python3
# -*- coding: utf-8 -*-


from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import sys


# creo la clase SmallSMILHandler y llamo a ContentHandler
class SmallSMILHandler(ContentHandler):  # ContentHandler
    list_tags = []
    root_layout = ['width', 'heigth', 'background_color']
    region = ['id', 'top', 'bottom', 'left', 'right']
    img = ['src', 'region', 'begin', 'dur']
    audio = ['src', 'begin', 'dur']
    textstream = ['src', 'region']

    dict = {
        'root_layout': root_layout,
        'region': region,
        'img': img,
        'audio': audio,
        'textstream': textstream
    }

    def get_tags(self):
        return self.list_tags

    def startElement(self, name, attrs):
        dict = {}
        if name in self.dict:
            dict['tag'] = name
            for atributo in self.dict[name]:
                dict[atributo] = attrs.get(atributo, '')
            self.list_tags.append(dict)


if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
   # 
