
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import urllib.request
import json
import sys
import smallsmilhandler
from xml.sax import make_parser


class KaraokeLocal:
    #inicializador:
    def __init__(self, file):
        parser = make_parser()
        cHandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(file))
        self.list_tags = cHandler.get_tags()

    def __str__(self):
        str_out = ''
        for dict in self.list_tags:
            tag_name = dict['tag']
            str_out += dict['tag']
            dict['tag'] = 'tag'
            str_out = self.get_tags_table(str_out, dict)
            dict['tag'] = tag_name
        return str_out

    def get_tags_table(self, str_out, dict):
        for key, value in dict.items():
            if key != dict['tag'] and value != "":
                str_out += '\t' + '{0}="{1}"'.format(key, value)
        str_out += '\n'
        return str_out

    def to_json(self, file):
        json_file = ''
        if json_file == '':
            json_file = file.replace('.smil', '.json')
        with open(json_file, 'w') as f:
            json.dump(self.list_tags, f, indent=4)

    def do_local(self):
        for dict in self.list_tags:
            for key, value in dict.items():
                if key == 'src' and value.startswith('http://'):
                    self.do_request(value, dict)


    def do_request(self, value, dict):
        local_file = value[value.rfind('/'):]
        urllib.request.urlretrieve(value, local_file[1:])
        dict['src'] = local_file[1:]


if __name__ == "__main__":
    try:
        file = sys.argv[1]
    except IndexError:
        sys.exit("Usage:python3 karaoke.py file.smil.")
    karaoke = KaraokeLocal(file)
    print(karaoke)
    karaoke.to_json(file)
    karaoke.do_local()
    karaoke.to_json('local.smil')
    print(karaoke)